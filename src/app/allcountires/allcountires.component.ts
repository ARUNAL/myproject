import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-allCountires',
  templateUrl: './allCountires.component.html',
  styleUrls: ['./allCountires.component.css']
})
export class allCountiresComponent implements OnChanges {
  @Input() allCountriesData: any;

  constructor() { }

  ngOnChanges() {
    console.log(this.allCountriesData)

    this.allCountriesData.sort((a, b) => {
      return b.TotalConfirmed - b.TotalConfirmed;

    })
    console.log(this.allCountriesData);
  }

}
