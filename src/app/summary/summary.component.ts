import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnChanges {
  @Input() globalData: any;

  constructor() {
    console.log(this.globalData)
  }
  ngOnChanges(): void {
    Object.keys(this.globalData).length;
    console.log(this.globalData)
  }

}
